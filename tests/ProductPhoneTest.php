<?php
declare(strict_types=1);

namespace App\Tests;

use App\ProductPhone;
use PHPUnit\Framework\TestCase;

class ProductPhoneTest extends TestCase
{
    public function testCreatePhoneWithDiagonal(): void
    {
        $TV = new ProductPhone(['diagonal' => 4.5]);
        $this->assertEquals($TV->getDiagonal(), 4.5);
    }
}