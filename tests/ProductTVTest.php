<?php
declare(strict_types=1);

namespace App\Tests;

use App\ProductTV;
use PHPUnit\Framework\TestCase;

class ProductTVTest extends TestCase
{
    public function testCreateTVWithDiagonal(): void
    {
        $TV = new ProductTV(['diagonal' => 4.5]);
        $this->assertEquals($TV->getDiagonal(), 4.5);
    }
}