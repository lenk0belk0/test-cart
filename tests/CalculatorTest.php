<?php
declare(strict_types=1);

namespace App\Tests;

use App\Calculator;
use App\Cart;
use App\ProductPan;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testCreateCalculator(): void
    {
        $calculator = Calculator::getInstance();
        $this->assertTrue($calculator instanceof Calculator);
    }

    public function testCalculatorCalculate()
    {
        $calculator = Calculator::getInstance();
        $cart = new Cart();
        $cart->add(new ProductPan(['price' => 10, 'brand' => 'lg']));

        $this->assertEquals(10, $calculator->calculate($cart));
    }

    /**
     * @expectedException \DomainException
     */
    public function testGotExceptionForCalculateInvalidCart(): void
    {
        $calculator = Calculator::getInstance();
        $cart = new Cart();
        $cart->add(new ProductPan(['brand' => 'lg']));

        $calculator->calculate($cart);
    }


}