<?php
declare(strict_types=1);

namespace App\Tests;

use App\ProductPan;
use PHPUnit\Framework\TestCase;

class ProductPanTest extends TestCase
{
    public function testCreateTVWithDiagonal(): void
    {
        $pan = new ProductPan(['diameter' => 25]);
        $this->assertEquals($pan->getDiameter(), 25);
    }
}