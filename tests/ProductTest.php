<?php
declare(strict_types=1);

namespace App\Tests;

use App\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testCreateProduct(): void
    {
        $product = new Product();
        $this->assertTrue($product instanceof Product);
    }

    public function testCreateProductWithPrice(): void
    {
        $product = new Product(['price' => 10]);
        $this->assertEquals($product->getPrice(), 10);

        $product = new Product(['price' => 100]);
        $this->assertEquals($product->getPrice(), 100);
    }

    public function testCreateProductWithBrand(): void
    {
        $product = new Product(['brand' => 'lg']);
        $this->assertEquals($product->getBrand(), 'lg');
    }

    public function testValidateProduct(): void
    {
        $product = new Product(['price' => 100, 'brand' => 'lg']);

        $this->assertTrue($product->validate());
    }

    public function testValidateInvalidProduct(): void
    {
        $product = new Product();

        $this->assertFalse($product->validate());
        $this->assertCount(2, $product->getErrors());
    }
}