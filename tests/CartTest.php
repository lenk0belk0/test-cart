<?php
declare(strict_types=1);

namespace App\Tests;

use App\Cart;
use App\Product;
use App\ProductPan;
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase
{
    public function testCreateCart(): void
    {
        $cart = new Cart();
        $this->assertTrue($cart instanceof Cart);
    }

    public function testAddProductToCart(): Cart
    {
        $cart = new Cart();
        $product = new Product();

        $cart->add($product);

        $this->assertCount(1, $cart->getProducts());

        return $cart;
    }

    public function testAddProductsToCart(): Cart
    {
        $cart = new Cart();
        $product = new Product();

        $cart->add($product, 2);

        $this->assertCount(2, $cart->getProducts());

        return $cart;
    }

    /**
     * @depends testAddProductsToCart
     */
    public function testGetProductFromCart(Cart $cart)
    {
        $product = $cart->getProduct(1);

        $this->assertTrue($product instanceof Product);
    }

    /**
     * @depends testAddProductsToCart
     */
    public function testGetProductWithInvalidKeyFromCart(Cart $cart)
    {
        $product = $cart->getProduct(10);

        $this->assertNull($product);
    }

    /**
     * @depends testAddProductsToCart
     */
    public function testRemoveProductWithInvalidKeyFromCart(Cart $cart)
    {
        $product = $cart->removeProduct(10);

        $this->assertNull($product);
    }

    /**
     * @depends testAddProductsToCart
     */
    public function testRemoveProductFromCart(Cart $cart)
    {
        $product = $cart->removeProduct(1);

        $this->assertTrue($product instanceof Product);
    }

    public function testValidateProductsInCart(): Cart
    {
        $cart = new Cart();
        $cart->add(new ProductPan(['price' => 10, 'brand' => 'lg']));

        $this->assertTrue($cart->validate());

        return $cart;
    }

    public function testValidateInvalidProductInCart(): Cart
    {
        $cart = new Cart();
        $cart->add(new ProductPan(['price' => 10]));

        $this->assertFalse($cart->validate());
        $this->assertCount(1, $cart->getErrors());

        return $cart;
    }
}