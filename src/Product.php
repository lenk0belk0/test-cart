<?php
declare(strict_types=1);

namespace App;

class Product
{
    protected $price;

    protected $brand;

    protected $errors;

    public function __construct(array $properties = [])
    {
        $this->errors = [];

        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function validate(): bool
    {
        $this->errors = [];

        if ($this->price === null || !is_int($this->price)) {
            $this->errors[] = "invalid price value \"{$this->price}\"";
        }

        if ($this->brand === null || !is_string($this->brand) || $this->brand === '') {
            $this->errors[] = "invalid brand value \"{$this->brand}\"";
        }

        return count($this->errors) == 0 ? true : false;
    }
}