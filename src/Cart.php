<?php
declare(strict_types=1);

namespace App;

class Cart
{
    private $products;
    private $errors;

    public function __construct()
    {
        $this->products = [];
        $this->errors = [];
    }

    public function add(Product $product, int $count = 1): void
    {
        for ($i = 0; $i < $count; $i++) {
            $this->products[] = clone $product;
        }
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function getProduct(int $key): ?Product
    {
        return isset($this->products[$key]) ? $this->products[$key] : null;
    }

    public function removeProduct(int $key): ?Product
    {
        if (!isset($this->products[$key])) {
            return null;
        }

        $removed = $this->products[$key];
        unset($this->products[$key]);

        return $removed;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function validate(): bool
    {
        $this->errors = [];
        /** @var Product $product */
        foreach ($this->products as $key => $product) {
            if (!$product->validate()) {
                $this->errors['Product-'.$key] = $product->getErrors();
            }
        }
        return count($this->errors) == 0 ? true : false;
    }
}