<?php
declare(strict_types=1);

namespace App;

class Calculator
{
    private static $instance;

    public static function getInstance(): Calculator
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function calculate(Cart $cart): int
    {
        if(!$cart->validate()) {
            throw new \DomainException('can\'t calculate invalid cart');
        }

        $sum = 0;
        /** @var Product $product */
        foreach ($cart->getProducts() as $product) {
            $sum += $product->getPrice();
        }
        return $sum;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}