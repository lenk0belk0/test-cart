<?php
declare(strict_types=1);

namespace App;

class ProductPan extends Product
{
    protected $diameter;

    public function getDiameter()
    {
        return $this->diameter;
    }
}