<?php
declare(strict_types=1);

namespace App;

trait ScreenProperties
{
    protected $diagonal;

    public function getDiagonal()
    {
        return $this->diagonal;
    }
}